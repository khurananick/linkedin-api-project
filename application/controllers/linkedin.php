<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Linkedin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		error_reporting(0);

		/*
		 * loading some files that hold business and helper logic.
		 */
		$this->load->helper('application_helper');
		$this->load->helper('apisettings_helper');
		$this->load->helper('view_helper');
		$this->load->model('linkedin_model', 'linkedin');
		
		/*
		 * profiler lets you view more details on the local environment.
		 */
		if(ENVIRONMENT == 'development')
			$this->output->enable_profiler(TRUE);
	}
	
	public function index()
	{
		/*
		 * if session expired, get new session data.
		 */ 
		if((!isset($this->linkedin->session_var['expires_at'])) || (time() > $this->linkedin->session_var['expires_at']))
		{
			$this->session->set_userdata('linkedin', '');
			$this->linkedin->session_var = array();
		}
		
		/*
		 * get authorization code if non available in current session.
		 */
		if(empty($this->linkedin->session_var)) $this->linkedin->getAuthorizationCode();

		/*
		 * send to dashboard with access token and state.
		 */
		redirect('linkedin/dashboard?code='.$this->linkedin->session_var['access_token'].'&state='.$this->linkedin->session_var['state']);
	}
	
	public function dashboard()
	{
		/*
		 * catch if api return error.
		 */
		if(isset($_GET['error'])) $this->linkedin->error_out();

		/*
		 * if api sent code, ensure that access token exists.
		 */
		elseif(isset($_GET['code']) && !isset($this->linkedin->session_var['access_token'])) $this->linkedin->matchAndGetAccessToken();
		
		/*
		 * get cached data.
		 */
		if(ENVIRONMENT == 'development' && file_exists('cached/cached.txt'))
			$data = unserialize(read_file('cached/cached.txt'));
		
		/*
		 * if no cached data, get data from api.
		 */
		if(!isset($data) || $data == '')
		{
			$api_data = $this->linkedin->get_all_dashboard_data();
			
			foreach($api_data as $key => $value) // format api as array to load views. 
				$data[$key] = $value;
			
			if(ENVIRONMENT == 'development')
				write_file('cached/cached.txt', serialize($data));
		}

		$this->linkedin->show_views($data);
	}
}
