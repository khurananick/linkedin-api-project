<?php

if(!function_exists('update_sess_data'))
{
	function update_sess_data($new_data)
	{
		$CI =& get_instance();
		$data = $CI->session->userdata('linkedin');

		if(!is_array($data)) $data = array();
		if(!is_array($new_data)) $new_data = array();
		
		foreach($new_data as $key => $value) $data[$key] = $value;

		$CI->session->set_userdata( array( 'linkedin' => $data ) );
		return $CI->session->userdata('linkedin');
	}
}