<?php

if(!function_exists('break_url'))
{
	function break_url($url)
	{
		$exploded = explode('&', $url);
		return $exploded[0];
	}
}

if(!function_exists('days_ago'))
{
	function days_ago($enddate)
	{
		$enddate = $enddate/1000;
		$today = strtotime(date('Y-m-d'));
		$datediff = $today - $end_date;
		return floor($datediff/(60*60*24));
	}
}

if(!function_exists('cut_off'))
{
	function cut_off($string)
	{
		if(strlen($string) > 20)
			$string = substr($string, 0, (20 - strlen($string))).'...';
		
		return $string;
	}
}

if(!function_exists('strip'))
{
	/*
	 * will change it to preg_replace. 
	 */
	function strip($string)
	{
		$string = trim($string);
		$string = str_replace('/', '', $string);
		$string = str_replace('-', '', $string);
		$string = str_replace('.', '', $string);
		$string = str_replace(':', '', $string);
		$string = str_replace(';', '', $string);
		$string = str_replace('&', '', $string);
		$string = str_replace(' ', '', $string);

		if(strlen($string) > 14)
			$string = substr($string, 0, (14 - strlen($string)));
		
		return $string;
	}
}
