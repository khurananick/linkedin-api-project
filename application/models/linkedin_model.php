<?php

class Linkedin_model extends CI_Model
{
	var $api_key;
	var $scope;
	var $api_secret;
	var $redirect_uri;
	var $session_var;
	
	public function __construct()
	{
		parent::__construct();

		/*
		 * setting often used variables.
		 * replace the functions below with your
		 * own app information.
		 */
		$this->api_secret	= secret_key();
		$this->api_key		= api_key();
		$this->scope		= scope();
		$this->redirect_uri	= base_url() . linkedin_redirect_path();
		$this->session_var	= $this->session->userdata('linkedin');		
	}
	
	public function show_views($data)
	{
		$data['title'] = 'Linkedin API App';
		
		$this->load->view('header', $data);
		$this->load->view('filters', $data);
		$this->load->view('news_dashboard', $data);
		$this->load->view('footer', $data);
	}
	
	public function error_out()
	{
		print $_GET['error'] . ': ' . $_GET['error_description'];
		exit;
	}

	public function matchAndGetAccessToken()
	{
		if($this->session_var['state'] === $_GET['state'])
		{
			$this->getAccessToken();
		}
		else
		{
			echo 'Invalid state provided.';
			exit;
		}
	}
	
	public function getAuthorizationCode()
	{
		$params = array('response_type' => 'code',
						'client_id' => $this->api_key,
						'scope' => $this->scope,
						'state' => uniqid('', true), // unique long string
						'redirect_uri' => $this->redirect_uri,
				  );
				  
		$url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params);
		update_sess_data($params);		
		redirect($url, 'location');
	}
	
	public function getAccessToken()
	{
		$params = array('grant_type' => 'authorization_code',
						'client_id' => $this->api_key,
						'client_secret' => $this->api_secret,
						'code' => $_GET['code'],
						'redirect_uri' => $this->redirect_uri,
				  );
		
		$url = 'https://www.linkedin.com/uas/oauth2/accessToken?' . http_build_query($params);
		
		$context = stream_context_create(
						array('http' => 
							array(
								'method' => 'POST'
							)
		                )
		            );

		$response = file_get_contents($url, true, $context);

		$token = json_decode($response);
		
		if($token != '')
		{
			$this->session_var['access_token'] = $token->access_token; // guard this! 
			$this->session_var['expires_in']   = $token->expires_in; // relative time (in seconds)
			$this->session_var['expires_at']   = time() + $this->session_var['expires_in']; // absolute time
			update_sess_data($this->session_var);
		}
		
		return true;
	}

	/*
	 * makes the call to the api with pre defined parameters and returns
	 * json returned by the api.
	 */
	public function fetch($method, $resource, $body = '')
	{
		$params = array('oauth2_access_token' => $this->session_var['access_token'],
						'format' => 'json',
				  );
		
		$operator = '?';
		
		if(preg_match('/' . preg_quote('?') . '/', $resource)) $operator = '&';

			$url = 'https://api.linkedin.com' . $resource . $operator . http_build_query($params);
			
		$context = stream_context_create(
						array('http' => 
							array('method' => $method,
		                    )
		                )
		            );

		$response = file_get_contents($url, false, $context);
		
		return json_decode($response);
	}
	
	/*
	 * returns the ids and names of groups the user belongs to.
	 */
	public function extract_groups($values)
	{
		$group_ids = new stdClass();
		
		foreach($values as $detail)
		{
			$group_id = $detail->group->id;
			$group_ids->$group_id->id = $detail->group->id;
			$group_ids->$group_id->name = $detail->group->name;
		}
		
		return $group_ids;
	}
	
	/*
	 * returns the ids and names of companies the user is following.
	 */
	public function extract_companies($values)
	{
		$extracted = new stdClass();
		
		foreach($values as $detail)
		{
			$company_id = $detail->id;
			$extracted->$company_id->id = $detail->id;
			$extracted->$company_id->name = $detail->name;
		}
		
		return $extracted;
	}
	
	public function get_all_dashboard_data()
	{
		$api = new stdClass();
		
		// fetch person data.
		$api->person = $this->fetch('GET', '/v1/people/~:(id,first-name,last-name,headline,picture-url,group-memberships,following)');
		
		// fetch person's newfeed.
		$api->stream = $this->fetch('GET', '/v1/people/~/network/updates?type=SHAR');
		
		// extract id,name from groups and companies the person belongs to/follows.
		$api->groups = $this->extract_groups($api->person->groupMemberships->values);
		$api->companies = $this->extract_companies($api->person->following->companies->values);

		// get posts by groups the user belongs to.
		foreach($api->groups as $group)
		{
			$group_id = $group->id;
			$api->group_stream->$group_id = $this->fetch('GET', "/v1/groups/$group_id/posts:(creation-timestamp,category,title,summary,creator:(first-name,last-name,picture-url,headline),likes,attachment:(image-url,content-domain,content-url,title,summary),relation-to-viewer)?order=recency&count=5");
		}
		
		// get status updates by companies the user follows.
		foreach($api->companies as $company)
		{
			$company_id = $company->id;
			$api->companies_stream->$company_id = $this->fetch('GET', "/v1/companies/$company_id/updates?event-type=status-update");
		}

		return $api;
	}
}
