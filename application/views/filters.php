			<div class="filters pull-left">
				<div class="filter">
					<p>Filter by Group</p>
					<?php foreach($groups as $value): ?>
						<label class="checkboxes">
							<input type="checkbox" data-id="<?=strip($value->name)?>" /> <?=cut_off($value->name)?>
						</label>
					<?php endforeach; ?>
				</div>
				<div class="filter">
					<p>Filter by Company</p>
					<?php foreach($companies as $value): ?>
						<label class="checkboxes">
							<input type="checkbox" data-id="<?=strip($value->name)?>" /> <?=cut_off($value->name)?>
						</label>
					<?php endforeach; ?>
				</div>
				<button class="btn btn-primary btn-small" id="reset_filters">Reset Filters</button>
			</div>
			
