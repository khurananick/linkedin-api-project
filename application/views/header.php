<html>
	<head>
		<title><?=$title?></title>

		<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" type="text/css" media="screen" />	
		<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css" type="text/css" media="screen" />	
		<link rel="stylesheet" href="<?=base_url()?>assets/css/style.css" type="text/css" media="screen" />	
		<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.slimscroll.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/functions.js"></script>
		<?php /*
		<script type="text/javascript" src="http://platform.linkedin.com/in.js">
		  api_key: azhvpkg4bnyr
		  authorize: true
		  onLoad: onLinkedInLoad
		  scope: r_basicprofile r_network
		</script>
		*/ ?>
	</head>
	<body>
		<div class="container">
			<div class="hero-unit">
				<h1>Hello, <?=$person->firstName.' '.$person->lastName?>!</h1>
				<p>We have gathered all the interesting things your network is talking about right here in this one simple screen. Check it out!</p>
			</div>
