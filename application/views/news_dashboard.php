			<div class="posts pull-right">
				<?php foreach($stream->values as $value): ?>
					<?php $shared = $value->updateContent->person->currentShare; ?>
					<?php if(isset($shared->content->title) && $shared->content->title != ''): ?>
						<div class="thumbnail box span4 pull-left mystream">
							<div class="content">
								<div class="header">
									<a class="black_link" href="<?=break_url($value->updateContent->person->siteStandardProfileRequest->url)?>"><?=$shared->author->firstName.' '.$shared->author->lastName?></a> shared:
								</div>
								<div class="title">
									<h2><a class="black_link" href="<?=$shared->content->submittedUrl?>" target="_blank"><?=$shared->content->title?></a></h2>
								</div>
				
								<div class="detail">
									<div class="image">
										<?php if(isset($shared->content->submittedImageUrl)): ?>
											<img src="<?=$shared->content->submittedImageUrl?>" />
										<?php elseif(isset($shared->content->thumbnailUrl)): ?>
											<img src="<?=$shared->content->thumbnailUrl?>" />
										<?php else: ?>
											<img src="<?=base_url()?>assets/img/no_photo.jpeg" />
										<?php endif; ?>
									</div>	
										
									<?=$shared->content->description?><br />
								</div>
				
								<div class="footer">
									<?php if(!isset($value->numLikes)) $value->numLikes = 0; ?>
									<span>Likes (<?=$value->numLikes?>) |</span>
									<span>Comments (<?=$value->updateComments->_total?>)</span>
								</div>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
		
				<?php foreach($group_stream as $group_id => $discussion): ?>
					<?php foreach($discussion->values as $value): ?>
						<?php if(isset($value->attachment)): ?>
							<div class="thumbnail box span4 pull-left groupstream <?=strip($groups->$group_id->name)?>" id="">
								<div class="header">
									<?=$value->creator->firstName.' '.$value->creator->lastName?> shared:
								</div>
								<div class="title">
									<h2><a class="black_link" href="<?=$value->attachment->contentUrl?>" target="_blank"><?=$value->attachment->title?></a></h2>
								</div>
				
								<div class="detail">
									<div class="image">
										<?php if(isset($value->attachment->imageUrl)): ?>
											<img src="<?=$value->attachment->imageUrl?>" />
										<?php else: ?>
											<img src="<?=base_url()?>assets/img/no_photo.jpeg" />
										<?php endif; ?>
									</div>
									<?=$value->summary?><br />
									<strong>Group:</strong> <?=$groups->$group_id->name?>
								</div>
				
								<div class="footer">
									<span>Likes (<?=$value->likes->_total?>)</span>
								</div>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endforeach; ?>
		
				<?php foreach($companies_stream->values as $value): ?>
					<?php $shared = $value->updateContent->companyStatusUpdate->share; ?>
					<?php if(isset($shared->content->title) && $shared->content->title != ''): ?>
						<div class="thumbnail box span4 pull-left companystream <?=strip($value->updateContent->company->name)?>" id="">
							<div class="content">
								<div class="header">
									<?=$value->updateContent->company->name?> shared:
								</div>
								<div class="title">
									<h2><a class="black_link" href="<?=$shared->content->submittedUrl?>" target="_blank"><?=$shared->content->title?></a></h2>
								</div>
				
								<div class="detail">
									<div class="image">
										<?php if(isset($shared->content->submittedImageUrl)): ?>
											<img src="<?=$shared->content->submittedImageUrl?>" />
										<?php elseif(isset($shared->content->thumbnailUrl)): ?>
											<img src="<?=$shared->content->thumbnailUrl?>" />
										<?php else: ?>
											<img src="<?=base_url()?>assets/img/no_photo.jpeg" />
										<?php endif; ?>
									</div>	
										
									<?=$shared->content->description?><br />
								</div>
				
								<div class="footer">
			
								</div>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
