
function displayProfilesErrors(error) {
	profilesDiv = document.getElementById("profiles");
	profilesDiv.innerHTML = "<p>Oops!</p>";
	console.log(error);
}

function displayPeopleSearch(peopleSearch) {
	var peopleSearchDiv = document.getElementById("peoplesearch");

	var members = peopleSearch.people.values; // people are stored in a different spot than earlier example
	
	for (var member in members){
		// but inside the loop, everything is the same
		// extract the title from the members first position
		peopleSearchDiv.innerHTML += "<p>" + members[member].firstName + " " + members[member].lastName + " is a " + members[member].positions.values[0].title + ".</p>";
	}
}

function displayConnections(connections){
	var connectionsDiv = document.getElementById("connections");
	var members = connections.values; // The list of members you are connected to

	for (var member in members) {
		connectionsDiv.innerHTML += "<p>" + members[member].firstName + " " + members[member].lastName + " works in the " + members[member].industry + " industry";
		console.log(members[member])
	}
}

function displayProfiles(profiles){
	member = profiles.values[0];
	document.getElementById("profiles").innerHTML = "<p id=\"" + member.id + "\">Hello " +  member.firstName + " " + member.lastName + "</p>";
	console.log(member);
}

function onLinkedInAuth(){
	IN.API.Profile("id=1FawDJh5j7").result(displayProfiles).error(displayProfilesErrors);
	console.log('function: onLinkedInAuth');
}

function onLinkedInAuthGetConnections(){
	IN.API.Connections("me").result(displayConnections).error(displayProfilesErrors);
	console.log('function: onLinkedInAuthGetConnections');
}

function peopleSearch(){
	IN.API.PeopleSearch()
	.fields("firstName", "lastName", "positions")
	.params({"title": "Product Manager", "current-title": true})
	.result(displayPeopleSearch)
	.error(displayPeopleSearchError);
}

function onLinkedInLoad(){
	IN.Event.on(IN, "auth", peopleSearch);
	console.log('function: onLinkedInLoad');
}
