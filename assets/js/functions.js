function hide_animated(classname)
{
	$("." + classname).first().hide("fast", function hideNext() {
		$(this).next("." + classname).hide("fast", hideNext);
	});
}

function show_animated(classname)
{
	$("." + classname).first().show("fast", function showNext() {
		$(this).next("." + classname).show("fast", showNext);
	});
}

function uncheck_all()
{
	var checkboxes = $('.filter input:checkbox');

	for(var i=0; i<checkboxes.length; i++){
		if(checkboxes[i] != undefined){
			checkboxes[i].checked = false;
		}
	}
}

function find_all_checked()
{
	var checkboxes = $('.filter input:checkbox');
	var checked = new Array();
	
	for(var i=0; i<checkboxes.length; i++){
		if(checkboxes[i] != undefined){
			if(checkboxes[i].checked == true){
				checked.push(checkboxes[i].dataset.id);
			}
		}
	}

	return checked;
}

function show_checked_or_all(checked)
{
	if(checked.length > 0){
		$('.box').hide();
		
		for(var i=0; i<checked.length; i++){
			show_animated(checked[i]);
		}
	}
	else{
		show_animated('box');
	}
}

function disable_unavailable_filter_options()
{
	var checkboxes = $('.filter input:checkbox');
	
	for(var i=0; i<checkboxes.length; i++){
		if($('.' + checkboxes[i].dataset.id).length == 0){
			var checkbox = $('input[data-id=' + checkboxes[i].dataset.id + ']');
			checkbox.attr('disabled', true);
			checkbox.parent().css('color', '#A4A4A4');
		}
	}
}

$(document).ready(function(){
	disable_unavailable_filter_options();
	
	$('.filter input:checkbox').click(function(){
		var all_checked = find_all_checked();
		show_checked_or_all(all_checked);
	});
	
	$('#reset_filters').click(function(){
		uncheck_all();
		show_checked_or_all(Array());
	});
});
